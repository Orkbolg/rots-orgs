:PROPERTIES:
:ID:       59d57675-c26f-45e6-a0e3-67633e18de23
:END:
#+title: an adamantine hammer head
#+author: Orkbolg
#+filetags: :rots:item:quest-item:evil:

* Description

This plain black hammer head is made of a dull metal that absorbs all
light. Two dark carvings of black and blood-red serpents can be seen upon the
sides of the hammer head. Both serpents are coiled and they have bright red
tongues which protrude as if the serpents are hissing.

* Quest

** Dark Side

This item is a part of the [[id:0307fe09-b78b-4ff0-8a94-91f62f95c505][a serpentine battle hammer]] quest from [[id:1d8d2437-8a9f-478f-a91b-dd4a62e1f209][Urszk the
Hammer]] in [[id:5e177325-5188-43e3-8f5a-ab52819a89f4][Dol-Guldur]].
