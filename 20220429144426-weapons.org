:PROPERTIES:
:ID:       298030ae-5dfa-4220-8a3a-f148d326fa93
:END:
#+title: Weapons
#+author: Orkbolg
#+filetags: rots weapons

* Piercing
** a light marble dagger :dagger:
:PROPERTIES:
:ID:       22a49d63-74c7-475a-80ca-1b1330508223
:END:

Cut from some strange black marble, this dagger is both light and durable.
The blade has been sharpened to a razor-sharp point, as the hilt has been
fashioned very carefully with several grooves making it easy to handle. White
marble streaks through the black like bolts of lightning.

*** Specifications

This weapon is made of stone, and weighs 1.2lbs.
This weapon can be taken and wielded.
The weapon you hold is a piercing weapon.

| Specification   | Value |
|-----------------+-------|
| Damage Rating   | 45/10 |
| Offensive Bonus |     0 |
| Parry Bonus     |     8 |
| Bulk            |     1 |

* Concussion

** a spiked ivory club :smiter:
:PROPERTIES:
:ID:       57b07b13-cc20-42b8-86b4-4830e4b0c24b
:END:

   This immense club of ivory has been fused with several sharp mithril points
along its outer surface. It's quite heavy and requires a strong individual to
wield it effectively. The ivory is extremely hard and durable, and the mithril
spikes add to the vast damage this club can give.

*** Specifications

- This weapon is made from precious mithril, and weighs 15.0lbs.
- This weapon can be taken and wielded.
- The weapon you hold is a smiting weapon.

| Specification   |  Value |
|-----------------+--------|
| Weight          | 15 lbs |
| Damage Rating   | 109/10 |
| Offensive Bonus |     10 |
| Parry Bonus     |    -15 |
| Bulk            |      8 |

*** Attributes

- It is magical in nature.

** an ivory mallet :smiter:
:PROPERTIES:
:ID:       1dca3074-3682-4006-9b62-7cd8356996da
:END:

   The handle of this mallet is rather short, made of metal and etched with
several fine letters. The large head of the mallet is pure ivory, fused with a
number of small mithril spikes. Combining the weight and power of the ivory,
and the piercing ability of the spikes, this is quite a dangerous weapon.

*** Specifications

- This weapon is made from precious mithril, and weighs 10.0lbs.
- This weapon can be taken and wielded.
- The weapon you hold is a smiting weapon.

| Specification   |  Value |
|-----------------+--------|
| Weight          | 10 lbs |
| Damage Rating   |  85/10 |
| Offensive Bonus |      7 |
| Parry Bonus     |      0 |
| Bulk            |      7 |

*** Attributes

- It is magical in nature.
