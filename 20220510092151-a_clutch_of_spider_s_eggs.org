:PROPERTIES:
:ID:       785e0964-574e-42cb-b9c3-e68e7a40d233
:END:
#+title: a clutch of spider's eggs
#+author: Orkbolg
#+filetags: :rots:item:quest-item:

* Description

This fairly large pouch is filled with innumerable spider's eggs.  The
small eggs look like white pearls, filled with thick, white gooey fluids.
Infant spiders can be seen forming within the eggs, which are around the size
of a pebble.  The clutch appears fragile, almost as if it was barely touched,
it would explode and the fluid would cover everything around.

* Quest

** Dark Side

This item is part of the [[id:51d0247d-7561-471e-a903-fd307b2caccf][A half-orc witch doctor]] quest in [[id:5e177325-5188-43e3-8f5a-ab52819a89f4][Dol-Guldur]] for
[[id:a1c90d50-e394-499e-8ae7-d6a86eab388f][a skull cup]]
