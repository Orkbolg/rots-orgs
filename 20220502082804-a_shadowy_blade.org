:PROPERTIES:
:ID:       604831da-d97b-4bee-b4ac-a4aa117f0d2c
:END:
#+title: a shadowy blade
#+author: Orkbolg
#+filetags: :rots:weapon:slashing:magical:

* Description

   This long, sharp sword seems to absorb any light that comes near it, making
the area surrounding the blade pitch black and hard to bring into focus.  The
blade is set upon a great, ornate hilt, inlaid with precious gems. It was
obviously once the weapon of a great warrior, somehow corrupted to darkness by
the forces of evil.

* Specification

This weapon is made of metal, and weighs 2.8lbs.
This weapon can be taken and wielded.
The weapon you hold is a slashing weapon.

| Specification   |   Value |
|-----------------+---------|
| Weight          | 2.8 lbs |
| Damage Rating   |   42/10 |
| Offensive Bonus |       5 |
| Parry Bonus     |      12 |
| Bulk            |       3 |


This item has the following attributes.
It is magical in nature.

This item has the following affections.
 -2 to MANA_REGEN
 -2 to WILLPOWER
