:PROPERTIES:
:ID:       625cab9c-c0d3-48c7-bda2-d22c80db2d74
:END:
#+title: a pale-green spider's fang
#+author: Orkbolg
#+filetags: rots weapon piercing magical

* Description

   The spider fang is of a pale-green hue, and seems to be coated in some sort
of virulent poison, though whether or not the poison is still effective is a
question left unanswered. The fang is relatively light, and looks as if it
would cause quite a good bit of damage were it to be lunged into the flesh of
an enemy.

* Specifications

- This weapon is made of the usual stuff, and weighs 0.8lbs.
- This weapon can be taken and wielded.
- The weapon you hold is a piercing weapon.

| Specification   |   Value |
|-----------------+---------|
| Weight          | 0.8 lbs |
| Damage Rating   |   51/10 |
| Offensive Bonus |      12 |
| Parry Bonus     |      -5 |
| Bulk            |       1 |

This item has no additional attributes.
