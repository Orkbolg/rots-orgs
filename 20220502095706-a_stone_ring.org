:PROPERTIES:
:ID:       c41ab014-68fc-4188-9dda-9cbe03b69391
:END:
#+title: a stone ring
#+author: Orkbolg
#+filetags: :rots:ring:finger:

* Description

   This ring has been crafted out of a strange, black stone.  The stone is so
deep black, and shiny, that all light is reflected off of it upon hitting it.
Yet at the same time, all light that is near it or around it becomes absorbed
into the deep blackness of the stone itself.  Along the band of the ring, fine
writing in a fair script has been engraved.  The words are hard to decipher,
for the darkness of the ring seems to engulf anything that is upon it.

* Specifications

This shield is made of stone, and weighs 0.1lbs.
This shield can be taken and worn on your finger.

| Specification |   Value |
|---------------+---------|
| Weight        | 0.1 lbs |
| Dodge Bonus   |       0 |
| Parry Bonus   |       1 |
| Encumberance  |       0 |
