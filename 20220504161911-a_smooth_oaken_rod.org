:PROPERTIES:
:ID:       7eb5bd54-6444-461b-bd75-0d097b6ff523
:END:
#+title: a smooth oaken rod
#+author: Orkbolg
#+filetags: :rots:item:quest-item:dg:goblin-gate:

* Description

   This rod of oak is extremely smooth, and with the correct craftsman and
some work looks as if it could easily be turned into quite a formidable and
strong staff of some sort. The wood itself looks extremely old and is
remarkably hardy.

* Specifications

This item is made of the usual stuff, and weighs 1.0lbs.
This item can be taken.

This item has the following attributes.
It cannot be stored for rent.

* Quest

** Dark Side

This item is obtain from [[id:a64122f1-f2a8-4931-ab4a-174f5d325833][A brutish orc]] in [[id:5e177325-5188-43e3-8f5a-ab52819a89f4][Dol-Guldur]].

** Light Side
