:PROPERTIES:
:ID:       dd9c8c60-599e-42d5-9a2e-b05c817c1aff
:END:
#+title: a plain golden ring
#+author: Orkbolg
#+filetags: :rots:finger:ring:

* Description

Small, and made from the highest quality gold, this ring has no inscription
- or indeed, any markings at all.  It seems to snugly fit the finger of
whoever wears it - large or small.

* Specifications

- This piece of armour is made of gold, and weighs 0.1lbs.
- This piece of armour can be taken and worn on your finger.

  | Specification  |   Value |
  |----------------+---------|
  | Weight         | 0.1 lbs |
  | Absorbtion     |       0 |
  | Min Absorbtion |       0 |
  | Encumberance   |       0 |
  | Dodge          |       2 |

This item has the following affections.
+2 to OB
-4 to WILLPOWER
