:PROPERTIES:
:ID:       3a815adb-8f02-461c-9fe1-feb5c9cbc626
:END:
#+title: a blue and gold emblazoned helmet
#+author: Orkbolg
#+filetags: :rots:armour:head:metal:

* Description

This large full helmet covers the head completely. Two narrow slits and a
few small breathing holes are the only openings. The helmet is extremely thick
and will resist the attempts of most weapons to puncture it. The weight and
restricted views make this piece of armour cumbersome, though its strength is
far superior to almost every other similar helmet. The helmet is made of
strong steel coated with golden leaf and  blue enamel combining to make a
beautiful yet robust pieces of armour. Upon either side of the head, large
finely sculptured wings have been added for decoration and to aid recognition
of the wearer. The wings resemble those of a dragon - the venation, membrane
and bones are beautifully detailed.

* Specifications

- This piece of armour is made of metal, and weighs 8.5lbs.
- This piece of armour can be taken and worn on the head.

| Specification  |   Value |
|----------------+---------|
| Weight         | 8.5 lbs |
| Absorbtion     |      66 |
| Min Absorbtion |       5 |
| Encumberance   |       7 |
| Dodge          |       0 |

* Quest

** Dark Side

This piece of armour is obtained from [[id:47ea7540-e006-4481-a2fd-418b078d4fed][Garik the armorer]] in [[id:5e177325-5188-43e3-8f5a-ab52819a89f4][Dol-Guldur]].

** Light Side
